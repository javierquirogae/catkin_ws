#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of Willow Garage, Inc. nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Revision $Id$

## Simple talker demo that published std_msgs/Strings messages
## to the 'chatter' topic

import rospy
from std_msgs.msg import String
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from tf import transformations
import math

regions = {'rr':0,'r':0,'f':0,'ff':0,}
comnd = Twist()

def callback(data): 
    global regions
   
    regions ={
        'rr': min(min(data.ranges[510:560]),10),
	'r': min(min(data.ranges[560:659]),10),
	'f': min(min(data.ranges[660:690]),10),
        'ff': min(min(data.ranges[700:720]),10),
    }
    com()
 
def com():
    global comnd
    global regions
    
    if regions['ff'] < 1 or regions['f'] < 0.7:
    	comnd.linear.x = 0
    else:
	comnd.linear.x = 1
    comnd.linear.y = 0
    comnd.linear.z = 0
    comnd.angular.x = 0
    comnd.angular.y = 0
    if  regions['f'] > 2:
    	comnd.angular.z = -2
    elif regions['f'] < 2 or regions['ff'] < 7:
	comnd.angular.z = 2
    elif regions['ff'] < 0.5 or regions['f'] < 0.5:
	comnd.angular.z = 0
    else:
	comnd.angular.z = 0

def talker():
    
    pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(20) # 5hz
    sub = rospy.Subscriber('scan', LaserScan, callback)
   
    

    while not rospy.is_shutdown():
 
        rospy.loginfo(comnd)
        rospy.loginfo('ff')
	rospy.loginfo(regions['ff'])
        rospy.loginfo('f')
	rospy.loginfo(regions['f'])
        rospy.loginfo('r')
	rospy.loginfo(regions['r'])
        pub.publish(comnd)
        rate.sleep()

        
if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
