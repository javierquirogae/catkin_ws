# CMake generated Testfile for 
# Source directory: /home/javier/catkin_ws/src
# Build directory: /home/javier/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("beginner_tutorials")
subdirs("my_rosbot_tutorial_pkg")
subdirs("ros_basics_tutorials")
subdirs("ros_essentials_cpp")
subdirs("turtlesim_cleaner")
subdirs("rosbot_description/src/rosbot_navigation")
subdirs("rosbot_description/src/rosbot_description")
subdirs("rosbot_description/src/rosbot_gazebo")
