# generated from catkin/cmake/em/order_packages.cmake.em

set(CATKIN_ORDERED_PACKAGES "")
set(CATKIN_ORDERED_PACKAGE_PATHS "")
set(CATKIN_ORDERED_PACKAGES_IS_META "")
set(CATKIN_ORDERED_PACKAGES_BUILD_TYPE "")
list(APPEND CATKIN_ORDERED_PACKAGES "beginner_tutorials")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "beginner_tutorials")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "my_rosbot_tutorial_pkg")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "my_rosbot_tutorial_pkg")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ros_basics_tutorials")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "ros_basics_tutorials")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "ros_essentials_cpp")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "ros_essentials_cpp")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "turtlesim_cleaner")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "turtlesim_cleaner")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosbot_navigation")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosbot_description/src/rosbot_navigation")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosbot_description")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosbot_description/src/rosbot_description")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "rosbot_gazebo")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "rosbot_description/src/rosbot_gazebo")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")

set(CATKIN_MESSAGE_GENERATORS )

set(CATKIN_METAPACKAGE_CMAKE_TEMPLATE "/usr/lib/python2.7/dist-packages/catkin_pkg/templates/metapackage.cmake.in")
