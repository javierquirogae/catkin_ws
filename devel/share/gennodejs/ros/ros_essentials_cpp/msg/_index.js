
"use strict";

let IoTSensor = require('./IoTSensor.js');
let FibonacciFeedback = require('./FibonacciFeedback.js');
let FibonacciActionGoal = require('./FibonacciActionGoal.js');
let FibonacciAction = require('./FibonacciAction.js');
let FibonacciResult = require('./FibonacciResult.js');
let FibonacciGoal = require('./FibonacciGoal.js');
let FibonacciActionFeedback = require('./FibonacciActionFeedback.js');
let FibonacciActionResult = require('./FibonacciActionResult.js');

module.exports = {
  IoTSensor: IoTSensor,
  FibonacciFeedback: FibonacciFeedback,
  FibonacciActionGoal: FibonacciActionGoal,
  FibonacciAction: FibonacciAction,
  FibonacciResult: FibonacciResult,
  FibonacciGoal: FibonacciGoal,
  FibonacciActionFeedback: FibonacciActionFeedback,
  FibonacciActionResult: FibonacciActionResult,
};
